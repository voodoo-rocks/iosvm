//
//  VMBaseResponse.m
//  RippIn
//
//  Created by Alexander Kryshtalev on 06.12.12.
//  Copyright (c) 2012 Voodoo Mobile. All rights reserved.
//

#import "VMBaseResponse.h"

@implementation VMBaseResponse

- (BOOL)isSucceeded
{
    return self.result.succeeded;
}

@end
