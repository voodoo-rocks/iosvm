//
//  Result.m
//  RippIn
//
//  Created by tihonov on 30.10.12.
//  Copyright (c) 2012 Voodoo Mobile. All rights reserved.
//

#import "RequestResult.h"
#import "Exception.h"

@implementation RequestResult

@synthesize succeeded, exceptions, code;

-(id)arrayClassForProperty:(NSString *)propertyName
{
    return [Exception class];
}

- (NSString *)exceptionsDescription
{
    NSString *result = @"";
    for (Exception *exception in exceptions) {
        result = [result stringByAppendingFormat:@"%@\n", exception.message];
    }
    return result;
}

@end
