//
//  JsonObjectWriter.h
//  BetYou
//
//  Created by Alexander Kryshtalev on 26.09.12.
//  Copyright (c) 2012 Voodoo Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JsonObjectWriter : NSObject

+ (NSDictionary*) dictionaryFromObject: (id) object withRoot: (NSString*) root;
+ (NSDictionary*) dictionaryFromObject: (id) object;
+ (NSString*) jsonFromObject: (id) object withRoot: (NSString*) root;

@end
