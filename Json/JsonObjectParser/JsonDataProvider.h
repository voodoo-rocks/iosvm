//
//  JsonDataProvider.h
//  BetYou
//
//  Created by Alexander Kryshtalev on 25.09.12.
//  Copyright (c) 2012 Voodoo Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol JsonDataProvider

- (id) arrayClassForProperty: (NSString*) propertyName;

@end
