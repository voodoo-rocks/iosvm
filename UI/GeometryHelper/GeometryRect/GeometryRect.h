//
//  GeometryRect.h
//  BetYou
//
//  Created by Olesya Kondrashkina on 12/20/12.
//  Copyright (c) 2012 Voodoo Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GeometryRect : NSObject

+ (CGRect)moveRect: (CGRect)rect toPoint: (CGPoint)point;
+ (CGRect)moveRect: (CGRect)rect bySize: (CGSize)size;
+ (CGRect)moveRectToZero: (CGRect)rect;

+ (CGRect)centerRect: (CGRect)rect intoRect: (CGRect)outerRect;
+ (CGRect)centerRect: (CGRect)rect atPoint: (CGPoint)point;
+ (CGRect)resizeRect: (CGRect)rect bySize: (CGSize)size;
+ (CGRect)resizeRect: (CGRect)rect setSize: (CGSize)size;

@end
