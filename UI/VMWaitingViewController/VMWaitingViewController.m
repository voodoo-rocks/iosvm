//
//  WaitingViewController.m
//  RippIn
//
//  Created by tihonov on 19.11.12.
//  Copyright (c) 2012 Voodoo Mobile. All rights reserved.
//

#import "VMWaitingViewController.h"

@implementation VMWaitingViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setText:(NSString *)infoText
{
    textLabel.text = infoText;
}

- (void)viewDidUnload {
    [super viewDidUnload];
}
@end
