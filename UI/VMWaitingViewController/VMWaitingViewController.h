//
//  WaitingViewController.h
//  RippIn
//
//  Created by tihonov on 19.11.12.
//  Copyright (c) 2012 Voodoo Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VMWaitingViewController : UIViewController
{
    IBOutlet UILabel *textLabel;    
}

@property (nonatomic, copy) NSString *text;

@end
