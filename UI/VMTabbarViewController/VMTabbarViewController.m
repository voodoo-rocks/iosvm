//
//  TabbarControllerViewController.m
//  RippIn
//
//  Created by Alexander Kryshtalev on 19.09.12.
//  Copyright (c) 2012 Voodoo Mobile. All rights reserved.
//

#import "VMTabbarViewController.h"
#import "GeometryRect.h"

@implementation VMTabbarViewController

@synthesize tabbarHeight, items, tabbarHidden, resizeClientFrame, popToRootOnClick;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
		
		items = [[NSMutableArray alloc] init];
		tabbarHidden = false;
		resizeClientFrame = false;
		popToRootOnClick = false;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
	
	for (int i = 0; i < items.count; ++i) {
		// TODO : replace with the option
		CGSize size = self.view.bounds.size;
		
		VMTabbarItem* item = [items objectAtIndex:i];
		int width = self.autosizeTabbar ? size.width / items.count : item.normalImage.size.width;
		
		int x = 0;
		if (!self.autosizeTabbar) {
			for (int j = 0; j < i; ++j) {
				x += [[items objectAtIndex:j] normalImage].size.width;
			}
		} else
			x = width * i;
		
		
		int height = self.autosizeTabbar ? self.tabbarHeight : item.normalImage.size.height;
		int y = size.height -  height;
		
		CGRect rect = CGRectMake(x, y, width, height);
		
		item.button = [UIButton buttonWithType:UIButtonTypeCustom];
		[item.button setAutoresizingMask:UIViewAutoresizingFlexibleTopMargin];
		
		item.button.frame = rect;
		[item.button setImage:item.normalImage forState:UIControlStateNormal];
		[item.button addTarget:self action:@selector(buttonPress:) forControlEvents:UIControlEventTouchUpInside];
        
        if (item.title && item.title.length > 0) {
            item.label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
            item.label.backgroundColor = [UIColor clearColor];
            item.label.font = self.titleFont;
            item.label.textColor = self.normalTitleColor;
            item.label.text = item.title;
            [item.label sizeToFit];
            item.label.frame = [GeometryRect centerRect:item.label.frame intoRect:[GeometryRect moveRectToZero:item.button.frame]];
            item.label.frame = [GeometryRect moveRect:item.label.frame toPoint:CGPointMake(item.label.frame.origin.x, item.button.frame.size.height - item.label.frame.size.height)];
            [item.button addSubview:item.label];
        }
		[self.view insertSubview:item.button atIndex:1]; // Higher than views
	}
	
	[self selectTabAtIndex:0 animated:NO];
}

- (void)buttonPress: (id) sender
{
	int index = [self controllerIndexByButton:sender];
	if (index != -1) {
        [self selectTabAtIndex: index animated:YES];
    }
}	
	
- (int) controllerIndexByButton: (id) sender
{
	for (int i = 0; i < items.count; i++) {
		VMTabbarItem* item = [items objectAtIndex:i];
		if (item.button == sender)
			return i;
	}
	
	return -1;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void) viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	
	[[selected getController] viewWillAppear:animated];
}

- (void) viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
	
	[[selected getController] viewDidAppear:animated];
}

- (void) viewWillDisappear:(BOOL)animated {
	[super viewWillDisappear:animated];
	
	[[selected getController] viewWillDisappear:animated];
}

- (void) viewDidDisappear:(BOOL)animated {
	[super viewDidDisappear:animated];
	
	[[selected getController] viewDidDisappear:animated];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void) addTabbarItemForController: (id) controllerClass normalImage: (UIImage*) normalImage selectedImage: (UIImage*) selectedImage
{
	VMTabbarItem* item = [[VMTabbarItem alloc] initForTabbarController:self
													   controllerClass:controllerClass
														   normalImage:normalImage
														 selectedImage:selectedImage];
	[items addObject:item];
}

- (void) addTabbarItemForController: (id) controllerClass normalImageName: (NSString*) normalImage selectedImageName: (NSString*) selectedImage
{
	VMTabbarItem* item = [[VMTabbarItem alloc] initForTabbarController:self
													   controllerClass:controllerClass
														   normalImage:[UIImage imageNamed:normalImage]
														 selectedImage:[UIImage imageNamed:selectedImage]];
	[items addObject:item];
}

- (void)addTabbarItemForController: (id) controllerClass normalImage: (UIImage*) normalImage selectedImage: (UIImage*) selectedImage withTitle:(NSString *)title;
{
    VMTabbarItem* item = [[VMTabbarItem alloc] initForTabbarController:self
                                                       controllerClass:controllerClass
                                                           normalImage:normalImage
                                                         selectedImage:selectedImage
                                                                 title:title
                                                      normalTitleColor:self.normalTitleColor
                                                    selectedTitleColor:self.selectedTitleColor];
    [items addObject:item];
}

- (void)addTabbarItemForController: (id) controllerClass normalImageName: (NSString*) normalImage selectedImageName: (NSString*) selectedImage withTitle:(NSString *)title;
{
    VMTabbarItem* item = [[VMTabbarItem alloc] initForTabbarController:self
                                                       controllerClass:controllerClass
                                                           normalImage:[UIImage imageNamed:normalImage]
                                                         selectedImage:[UIImage imageNamed:selectedImage]
                                                                 title:title
                                                      normalTitleColor:self.normalTitleColor
                                                    selectedTitleColor:self.selectedTitleColor];
    [items addObject:item];
}

- (void) selectTabAtIndex: (NSUInteger) index animated:(BOOL) animated {
	VMTabbarItem* newSelection =[items objectAtIndex:index];
	
	if (selected != nil && selected == newSelection) {
		[[selected getController] popToRootViewControllerAnimated:YES];
		return;
	}
	
	if (self.popToRootOnClick) {
		[[selected getController] popToRootViewControllerAnimated:YES];
	}
	
	UINavigationController* newController = [newSelection getController];
	
	newController.view.frame = [self clientFrame];
	
	[self.view insertSubview:newController.view atIndex:0];
	[newSelection setSelected:true];
	
	if (selected != nil)
	{
		UINavigationController* controller = [selected getController];
		[controller viewWillDisappear: YES];
		[controller.view removeFromSuperview];
		[selected setSelected:false];
	}
	
	[newController viewWillAppear:YES];
	
	[self transitionFromViewController: [selected getController]
					  toViewController:newController
						withTransition:UIViewAnimationOptionTransitionCurlDown
							completion:^(BOOL finished) {
								selected = newSelection;
							}];
}

- (UINavigationController *) navigationControllerAtIndex: (NSUInteger) index;
{
	return [selected getController];
}

- (void)transitionFromViewController:(UIViewController *) fromController
					toViewController:(UIViewController *) toController
					  withTransition:(UIViewAnimationOptions)transition
						  completion:(void (^)(BOOL finished))completion
{
    [UIView transitionFromView:fromController.view
                        toView:toController.view
                      duration:0.4f
                       options:transition
                    completion:completion];
}

- (void)setTabbarHidden:(BOOL)_tabbarHidden
{
	if (!tabbarHidden && _tabbarHidden) {
		[self hideTabbar];
	}
	
	if (tabbarHidden && !_tabbarHidden) {
		[self showTabbar];
	}
}

- (int)realTabbarHeight
{
	int offset = [self tabbarHeight];
	if (!self.autosizeTabbar) {
		for (VMTabbarItem *item in self.items) {
			offset = MAX(item.button.frame.size.height, offset);
		}
	}
	return offset;
}


- (void)hideTabbar
{
	tabbarHidden = TRUE;
	int offset = [self realTabbarHeight ];
	
	[UIView animateWithDuration:0.3 animations:^{
		for (VMTabbarItem *item in self.items) {
			item.button.frame = CGRectOffset(item.button.frame, 0, offset);
		}
	}];
}

- (void)showTabbar
{
	tabbarHidden = FALSE;
	int offset = [self realTabbarHeight ];
	
	[UIView animateWithDuration:0.3 animations:^{
		for (VMTabbarItem *item in self.items) {
			item.button.frame = CGRectOffset(item.button.frame, 0, -offset);
		}
	}];
}

- (BOOL)tabbarHidden
{
	return tabbarHidden;
}

- (CGRect)clientFrame
{
	CGRect rect = self.view.bounds;
	if (resizeClientFrame)
		rect = CGRectMake(rect.origin.x, rect.origin.y, rect.size.width, rect.size.height - [self realTabbarHeight]);
	return rect;
}

@end
