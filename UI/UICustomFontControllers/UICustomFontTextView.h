//
//  UICustomFontTextView.h
//  NDA
//
//  Created by Dmitry Tihonov on 25.01.13.
//  Copyright (c) 2013 Voodoo Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UICustomFontTextView : UITextView

@property (nonatomic) NSString *customFont;

@end
