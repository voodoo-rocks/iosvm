//
//  BaseValidator.h
//  BetYou
//
//  Created by Alexander Kryshtalev on 19.11.12.
//  Copyright (c) 2012 Voodoo Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VMValidatorDelegate.h"

@interface VMBaseValidator : NSObject
{

}

- (BOOL)validateInput: (id)input;
- (BOOL)validate;

@property id <VMValidatorDelegate> delegate;
@property IBOutletCollection(id) NSMutableArray *validatingInputs;
@property IBOutlet UIView *ownerView;

@end
