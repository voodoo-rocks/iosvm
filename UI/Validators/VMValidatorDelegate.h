//
//  ValidatorDelegate.h
//  BetYou
//
//  Created by Alexander Kryshtalev on 19.11.12.
//  Copyright (c) 2012 Voodoo Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol VMValidatorDelegate <NSObject>

- (void)willValidate;
- (void)didValidate: (NSUInteger) errorsCount;

@end
