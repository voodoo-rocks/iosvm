//
//  VMUrlImageView.h
//  Prodensa
//
//  Created by Alexander Kryshtalev on 24.12.12.
//  Copyright (c) 2012 Voodoo Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASIHTTPRequest.h"

@interface VMUrlImageView : UIImageView
{
	__weak ASIHTTPRequest *request;
    UIActivityIndicatorView *activity;
}

- (void) cancelRequest;

- (void)setImageWithURL:(NSURL *)url placeholderImage:(UIImage *)placeholder;

@property (nonatomic) UIColor *activityIndicatorColor;

@property (nonatomic) CGFloat requestTimeOut;

@property BOOL strongCache;

@end
