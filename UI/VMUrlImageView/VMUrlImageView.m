//
//  VMUrlImageView.m
//  Prodensa
//
//  Created by Alexander Kryshtalev on 24.12.12.
//  Copyright (c) 2012 Voodoo Mobile. All rights reserved.
//

#import "VMUrlImageView.h"
#import "ASIDownloadCache.h"
#import "Reachability.h"

@implementation VMUrlImageView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    activity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    activity.center = self.center;
    activity.color = _activityIndicatorColor;
    [self.superview addSubview:activity];
}

-(void)dealloc {

}

- (void)setActivityIndicatorColor:(UIColor *)activityIndicatorColor
{
    _activityIndicatorColor = activityIndicatorColor;
    activity.color = activityIndicatorColor;
}

- (void) cancelRequest
{
    [request clearDelegatesAndCancel];
}

- (void)setRequestTimeOut:(CGFloat)requestTimeOut
{
    request.timeOutSeconds = requestTimeOut;
}

- (void)setImageWithURL:(NSURL *)url placeholderImage:(UIImage *)placeholder {

	if (placeholder) {
        self.image = placeholder;
	} else {
        self.image = nil;
        [activity startAnimating];
    }
	
    [request clearDelegatesAndCancel];
    
    request = [ASIHTTPRequest requestWithURL:url];
    [request setDownloadCache:[ASIDownloadCache sharedCache]];
	request.cacheStoragePolicy = ASICachePermanentlyCacheStoragePolicy;
    
    if (![Reachability reachabilityForInternetConnection].isReachable) {
        [request setCachePolicy:ASIOnlyLoadIfNotCachedCachePolicy];
    }
    
    if (self.strongCache)
    {
        [request setCachePolicy:ASIOnlyLoadIfNotCachedCachePolicy];
    }
    
	[request setCompletionBlock:^{
        
        [activity stopAnimating];
        
		if (request.responseStatusCode != 200)
			return;
		
		self.image = [UIImage imageWithData:request.responseData];

	}];
    
    [request setFailedBlock:^{
        [activity stopAnimating];
    }];
	
	[request startAsynchronous];
}

@end
