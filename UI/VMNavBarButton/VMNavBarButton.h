//
//  VMNavBarButton.h
//  GSPrice
//
//  Created by Alexander Kryshtalev on 28.09.13.
//  Copyright (c) 2013 Voodoo Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VMNavBarButton : UIButton

@property int ios7offset;
@property int ios6offset;

@end
