//
//  NSBubbleData.m
//
//  Created by Alex Barinov
//  Project home page: http://alexbarinov.github.com/UIBubbleTableView/
//
//  This work is licensed under the Creative Commons Attribution-ShareAlike 3.0 Unported License.
//  To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/3.0/
//

#import "NSBubbleData.h"
#import <QuartzCore/QuartzCore.h>
#import <MediaPlayer/MediaPlayer.h>

@implementation NSBubbleData

#pragma mark - Properties

@synthesize date = _date;
@synthesize type = _type;
@synthesize view = _view;
@synthesize insets = _insets;
@synthesize avatar = _avatar;
@synthesize attachmentDataName;

#pragma mark - Lifecycle

#if !__has_feature(objc_arc)
- (void)dealloc
{
    [_date release];
	_date = nil;
    [_view release];
    _view = nil;
    
    self.avatar = nil;

    [super dealloc];
}
#endif

+ (id)dataWithBlob:(QBCBlob *)blob data:(NSData *)data date:(NSDate *)date type:(NSBubbleType)type
{
    NSBubbleData *bubble = nil;
    if ([blob.contentType rangeOfString:@"image"].location != NSNotFound){
        bubble = [NSBubbleData dataWithImage:[UIImage imageWithData:data] date:date type:type];
    } else if ([blob.contentType rangeOfString:@"video"].location != NSNotFound){
        bubble = [NSBubbleData dataWithVideo:data name:blob.name date:date type:type];
    } else {
        bubble = [NSBubbleData dataWithAudio:data name:blob.name date:date type:type];
    }
    return bubble;
}

#pragma mark - Text bubble

const UIEdgeInsets textInsetsMine = {5, 10, 11, 17};
const UIEdgeInsets textInsetsSomeone = {5, 15, 11, 10};

+ (id)dataWithText:(NSString *)text date:(NSDate *)date type:(NSBubbleType)type
{
#if !__has_feature(objc_arc)
    return [[[NSBubbleData alloc] initWithText:text date:date type:type] autorelease];
#else
    return [[NSBubbleData alloc] initWithText:text date:date type:type];
#endif    
}

- (id)initWithText:(NSString *)text date:(NSDate *)date type:(NSBubbleType)type
{
    UIFont *font = [UIFont systemFontOfSize:[UIFont systemFontSize]];
    CGSize size = [(text ? text : @"") sizeWithFont:font constrainedToSize:CGSizeMake(220, 9999) lineBreakMode:NSLineBreakByWordWrapping];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, size.width, size.height)];
    label.numberOfLines = 0;
    label.lineBreakMode = NSLineBreakByWordWrapping;
    label.text = (text ? text : @"");
    label.font = font;
    label.backgroundColor = [UIColor clearColor];
    
#if !__has_feature(objc_arc)
    [label autorelease];
#endif
    
    UIEdgeInsets insets = (type == BubbleTypeMine ? textInsetsMine : textInsetsSomeone);
    return [self initWithView:label date:date type:type insets:insets];
}

#pragma mark - Audio bubble

+ (id)dataWithAudio:(NSData *)audioData name:(NSString *)name date:(NSDate *)date type:(NSBubbleType)type
{
    return [[NSBubbleData alloc] initWithAudio:audioData name:(NSString *)name date:date type:type];
}

- (id)initWithAudio:(NSData *)audioData name:(NSString *)name date:(NSDate *)date type:(NSBubbleType)type
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingFormat:@"/%@.caf", name];
    
    [audioData writeToFile:filePath atomically:YES];
        
    NSBubbleData *newObject = [self initWithText:@"Audio mesage. Tap to listen" date:date type:type];
    newObject.attachmentDataName = name;
    [newObject.view addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:newObject action:@selector(onPlayAudio:)]];
    newObject.view.userInteractionEnabled = YES;
    
    return newObject;
}

- (void)onPlayAudio:(UITapGestureRecognizer *)tapGestureRecognizer
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingFormat:@"/%@.caf", self.attachmentDataName];
    
    NSURL *mediaURL = [NSURL fileURLWithPath:filePath isDirectory:NO];
    
    NSError *error;
    audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:mediaURL error:&error];
    [audioPlayer play];    
}

#pragma mark - Video bubble

const UIEdgeInsets imageInsetsMine = {11, 13, 16, 22};
const UIEdgeInsets imageInsetsSomeone = {11, 18, 16, 14};

+ (id)dataWithVideo:(NSData *)videoData name:(NSString *)name date:(NSDate *)date type:(NSBubbleType)type
{
    return [[NSBubbleData alloc] initWithVideo:videoData name:(NSString *)name date:date type:type];
}

- (id)initWithVideo:(NSData *)videoData name:(NSString *)name date:(NSDate *)date type:(NSBubbleType)type
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingFormat:@"/%@.mp4", name];
    
    NSBubbleData *newObject = nil;
    UIImage *thumbnail = [UIImage imageNamed:@"no-profile-pic"];
    
    if ([videoData writeToFile:filePath atomically:YES]){
        NSURL *mediaURL = [NSURL fileURLWithPath:filePath isDirectory:NO];
        MPMoviePlayerController *tempPlayer = [[MPMoviePlayerController alloc] initWithContentURL:mediaURL];
        thumbnail = [tempPlayer thumbnailImageAtTime:0.0 timeOption:MPMovieTimeOptionExact];
        [tempPlayer stop];
    }
    newObject = [self initWithImage:thumbnail date:date type:type];
    newObject.attachmentDataName = name;
    [newObject.view addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:newObject action:@selector(onPlayVideo:)]];
    newObject.view.userInteractionEnabled = YES;
    
    return newObject;
}

- (void)onPlayVideo:(UITapGestureRecognizer *)tapGestureRecognizer
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingFormat:@"/%@.mp4", self.attachmentDataName];
    
    NSURL *mediaURL = [NSURL fileURLWithPath:filePath isDirectory:NO];
    
    videoPlayer = [[MPMoviePlayerController alloc] initWithContentURL:mediaURL];
    UIViewController *top = [[AppDelegate sharedDelegate] topUIViewController];
    [top.view addSubview:videoPlayer.view];
    videoPlayer.fullscreen = YES;
    [videoPlayer play];
}

#pragma mark - Image bubble

+ (id)dataWithImage:(UIImage *)image date:(NSDate *)date type:(NSBubbleType)type
{
#if !__has_feature(objc_arc)
    return [[[NSBubbleData alloc] initWithImage:image date:date type:type] autorelease];
#else
    return [[NSBubbleData alloc] initWithImage:image date:date type:type];
#endif    
}

- (id)initWithImage:(UIImage *)image date:(NSDate *)date type:(NSBubbleType)type
{
    CGSize size = image.size;
    if (size.width > 220)
    {
        size.height /= (size.width / 220);
        size.width = 220;
    }
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, size.width, size.height)];
    imageView.image = image;
    imageView.layer.cornerRadius = 5.0;
    imageView.layer.masksToBounds = YES;

    
#if !__has_feature(objc_arc)
    [imageView autorelease];
#endif
    
    UIEdgeInsets insets = (type == BubbleTypeMine ? imageInsetsMine : imageInsetsSomeone);
    return [self initWithView:imageView date:date type:type insets:insets];
}

#pragma mark - Custom view bubble

+ (id)dataWithView:(UIView *)view date:(NSDate *)date type:(NSBubbleType)type insets:(UIEdgeInsets)insets
{
#if !__has_feature(objc_arc)
    return [[[NSBubbleData alloc] initWithView:view date:date type:type insets:insets] autorelease];
#else
    return [[NSBubbleData alloc] initWithView:view date:date type:type insets:insets];
#endif    
}

- (id)initWithView:(UIView *)view date:(NSDate *)date type:(NSBubbleType)type insets:(UIEdgeInsets)insets  
{
    self = [super init];
    if (self)
    {
#if !__has_feature(objc_arc)
        _view = [view retain];
        _date = [date retain];
#else
        _view = view;
        _date = date;
#endif
        _type = type;
        _insets = insets;
    }
    return self;
}

@end
